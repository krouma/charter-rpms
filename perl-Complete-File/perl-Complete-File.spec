#
# This SPEC file was automatically generated using the cpantorpm
# script.
#
#    Package:           perl-Complete-File
#    Version:           0.443
#    cpantorpm version: 1.13
#    Date:              Thu Aug 03 2023
#    Command:
# /usr/local/bin/cpantorpm -T vendor Complete\:\:File
#

Name:           perl-Complete-File
Version:        0.443
Release:        1%{?dist}
Summary:        Completion routines related to files
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Complete-File/
BugURL:         http://search.cpan.org/dist/Complete-File/
BuildArch:      noarch
Source0:        https://cpan.metacpan.org/authors/id/P/PE/PERLANCAR/Complete-File-%{version}.tar.gz

#
# Unfortunately, the automatic provides and requires do NOT always work (it
# was broken on the very first platform I worked on).  We'll get the list
# of provides and requires manually (using the RPM tools if they work, or
# by parsing the files otherwise) and manually specify them in this SPEC file.
#

AutoReqProv:    no
AutoReq:        no
AutoProv:       no

Provides:       perl(Complete::File) = 0.443
Requires:       perl >= 5.010001
Requires:       perl(Complete::Common) >= 0.22
Requires:       perl(Complete::Path) >= 0.251
Requires:       perl(Complete::Util) >= 0.608
Requires:       perl(Exporter) >= 5.57
Requires:       perl(Encode)
Requires:       perl(File::Glob)
Requires:       perl(strict)
Requires:       perl(warnings)
BuildRequires:  perl >= 5.010001
BuildRequires:  perl(Complete::Common) >= 0.22
BuildRequires:  perl(Complete::Path) >= 0.251
BuildRequires:  perl(Complete::Util) >= 0.608
BuildRequires:  perl(Exporter) >= 5.57
BuildRequires:  perl(Encode)
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(File::Glob)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
A perl module

%prep

rm -rf %{_builddir}/Complete-File-%{version}
%setup -D -n Complete-File-0.443
chmod -R u+w %{_builddir}/Complete-File-%{version}

if [ -f pm_to_blib ]; then rm -f pm_to_blib; fi

%build

%{__perl} Makefile.PL OPTIMIZE="$RPM_OPT_FLAGS" INSTALLDIRS=vendor INSTALLVENDORBIN=%{_bindir} INSTALLVENDORSCRIPT=%{_bindir} INSTALLVENDORMAN1DIR=%{_mandir}/man1 INSTALLVENDORMAN3DIR=%{_mandir}/man3 INSTALLSCRIPT=%{_bindir}
make %{?_smp_mflags}

%install

rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;
%{_fixperms} $RPM_BUILD_ROOT/*

%clean

rm -rf $RPM_BUILD_ROOT

%files

%defattr(-,root,root,-)
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Thu Aug 03 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> 0.443-1
- Generated using cpantorpm
