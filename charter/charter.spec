Name:           charter
Version:        0.3.0
Release:        %autorelease
Summary:        A tool for Helm chart generation

License:        GPLv3
URL:            https://gitlab.com/krouma/charter
Source0:        https://gitlab.com/api/v4/projects/48056819/packages/generic/%{name}/%{version}/%{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  perl-generators
BuildRequires:  make

%{?perl_default_filter}

%description
The charter is a tool that generates a very customizable Helm chart from a single YAML file. Charter supports multiple workloads (e.g. Deployments) in a single charts.


%prep
%autosetup


%build
%configure
%make_build


%install
%make_install


%check


%files
%license COPYING
%doc ChangeLog.md README.md
%{_bindir}/%{name}
%dir %{_datadir}/templates
%{_datadir}/templates/%{name}


%changelog
* Sun Aug 06 2023 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Add ChangeLog
- Remove explicit Requires
- Use %%configure macro
- Change source
- Update to 0.3.0

* Thu Aug 03 2023 Matyáš Kroupa <kroupa.matyas@gmail.com>
- Initial version
