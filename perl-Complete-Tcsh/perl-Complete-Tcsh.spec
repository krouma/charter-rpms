#
# This SPEC file was automatically generated using the cpantorpm
# script.
#
#    Package:           perl-Complete-Tcsh
#    Version:           0.030
#    cpantorpm version: 1.13
#    Date:              Thu Aug 03 2023
#    Command:
# /usr/local/bin/cpantorpm Complete\:\:Tcsh
#

Name:           perl-Complete-Tcsh
Version:        0.030
Release:        1%{?dist}
Summary:        Completion module for tcsh shell
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Complete-Tcsh/
BugURL:         http://search.cpan.org/dist/Complete-Tcsh/
BuildArch:      noarch
Source0:        https://cpan.metacpan.org/authors/id/P/PE/PERLANCAR/Complete-Tcsh-%{version}.tar.gz

#
# Unfortunately, the automatic provides and requires do NOT always work (it
# was broken on the very first platform I worked on).  We'll get the list
# of provides and requires manually (using the RPM tools if they work, or
# by parsing the files otherwise) and manually specify them in this SPEC file.
#

AutoReqProv:    no
AutoReq:        no
AutoProv:       no

Provides:       perl(Complete::Tcsh) = 0.030
Requires:       perl >= 5.010001
Requires:       perl(Complete::Bash) >= 0.331
Requires:       perl(Exporter) >= 5.57
Requires:       perl(strict)
Requires:       perl(warnings)
BuildRequires:  perl >= 5.010001
BuildRequires:  perl(Complete::Bash) >= 0.331
BuildRequires:  perl(Exporter) >= 5.57
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
tcsh allows completion to come from various sources. One of the simplest is
from a list of words:

%prep

rm -rf %{_builddir}/Complete-Tcsh-%{version}
%setup -D -n Complete-Tcsh-0.030
chmod -R u+w %{_builddir}/Complete-Tcsh-%{version}

if [ -f pm_to_blib ]; then rm -f pm_to_blib; fi

%build

%{__perl} Makefile.PL OPTIMIZE="$RPM_OPT_FLAGS" INSTALLDIRS=vendor INSTALLSITEBIN=%{_bindir} INSTALLSITESCRIPT=%{_bindir} INSTALLSITEMAN1DIR=%{_mandir}/man1 INSTALLSITEMAN3DIR=%{_mandir}/man3 INSTALLSCRIPT=%{_bindir}
make %{?_smp_mflags}

#
# This is included here instead of in the 'check' section because
# older versions of rpmbuild (such as the one distributed with RHEL5)
# do not do 'check' by default.
#

if [ -z "$RPMBUILD_NOTESTS" ]; then
   make test
fi

%install

rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;
%{_fixperms} $RPM_BUILD_ROOT/*

%clean

rm -rf $RPM_BUILD_ROOT

%files

%defattr(-,root,root,-)
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Thu Aug 03 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> 0.030-1
- Generated using cpantorpm
