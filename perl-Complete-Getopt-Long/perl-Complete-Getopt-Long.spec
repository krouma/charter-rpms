#
# This SPEC file was automatically generated using the cpantorpm
# script.
#
#    Package:           perl-Complete-Getopt-Long
#    Version:           0.481
#    cpantorpm version: 1.13
#    Date:              Thu Aug 03 2023
#    Command:
# /usr/local/bin/cpantorpm Complete\:\:Getopt\:\:Long
#

Name:           perl-Complete-Getopt-Long
Version:        0.481
Release:        1%{?dist}
Summary:        Complete command-line argument using Getopt::Long specification
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Complete-Getopt-Long/
BugURL:         http://search.cpan.org/dist/Complete-Getopt-Long/
BuildArch:      noarch
Source0:        https://cpan.metacpan.org/authors/id/P/PE/PERLANCAR/Complete-Getopt-Long-%{version}.tar.gz

#
# Unfortunately, the automatic provides and requires do NOT always work (it
# was broken on the very first platform I worked on).  We'll get the list
# of provides and requires manually (using the RPM tools if they work, or
# by parsing the files otherwise) and manually specify them in this SPEC file.
#

AutoReqProv:    no
AutoReq:        no
AutoProv:       no

Provides:       perl(Complete::Getopt::Long) = 0.481
Requires:       perl >= 5.010001
Requires:       perl(Complete::File) >= 0.440
Requires:       perl(Complete::Util) >= 0.608
Requires:       perl(Exporter) >= 5.57
Requires:       perl(Getopt::Long::Util) >= 0.896
Requires:       perl(Log::ger) >= 0.038
Requires:       perl(String::Wildcard::Bash) >= 0.044
Requires:       perl(Complete::Env)
Requires:       perl(strict)
Requires:       perl(warnings)
BuildRequires:  perl >= 5.010001
BuildRequires:  perl(Complete::File) >= 0.440
BuildRequires:  perl(Complete::Util) >= 0.608
BuildRequires:  perl(Exporter) >= 5.57
BuildRequires:  perl(Getopt::Long::Util) >= 0.896
BuildRequires:  perl(Log::ger) >= 0.038
BuildRequires:  perl(String::Wildcard::Bash) >= 0.044
BuildRequires:  perl(Complete::Env)
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
A perl module

%prep

rm -rf %{_builddir}/Complete-Getopt-Long-%{version}
%setup -D -n Complete-Getopt-Long-0.481
chmod -R u+w %{_builddir}/Complete-Getopt-Long-%{version}

if [ -f pm_to_blib ]; then rm -f pm_to_blib; fi

%build

%{__perl} Makefile.PL OPTIMIZE="$RPM_OPT_FLAGS" INSTALLDIRS=vendor INSTALLSITEBIN=%{_bindir} INSTALLSITESCRIPT=%{_bindir} INSTALLSITEMAN1DIR=%{_mandir}/man1 INSTALLSITEMAN3DIR=%{_mandir}/man3 INSTALLSCRIPT=%{_bindir}
make %{?_smp_mflags}

%install

rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;
%{_fixperms} $RPM_BUILD_ROOT/*

%clean

rm -rf $RPM_BUILD_ROOT

%files

%defattr(-,root,root,-)
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Thu Aug 03 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> 0.481-1
- Generated using cpantorpm
