#
# This SPEC file was automatically generated using the cpantorpm
# script.
#
#    Package:           perl-Getopt-Long-Subcommand
#    Version:           0.104
#    cpantorpm version: 1.13
#    Date:              Thu Aug 03 2023
#    Command:
# /usr/local/bin/cpantorpm Getopt\:\:Long\:\:Subcommand
#

Name:           perl-Getopt-Long-Subcommand
Version:        0.104
Release:        1%{?dist}
Summary:        Process command-line options, with subcommands and completion
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Getopt-Long-Subcommand/
BugURL:         http://search.cpan.org/dist/Getopt-Long-Subcommand/
BuildArch:      noarch
Source0:        https://cpan.metacpan.org/authors/id/P/PE/PERLANCAR/Getopt-Long-Subcommand-%{version}.tar.gz

#
# Unfortunately, the automatic provides and requires do NOT always work (it
# was broken on the very first platform I worked on).  We'll get the list
# of provides and requires manually (using the RPM tools if they work, or
# by parsing the files otherwise) and manually specify them in this SPEC file.
#

AutoReqProv:    no
AutoReq:        no
AutoProv:       no

Provides:       perl(Getopt::Long::Subcommand) = 0.104
Requires:       perl >= 5.010001
Requires:       perl(Complete::Bash) >= 0.333
Requires:       perl(Complete::Getopt::Long) >= 0.479
Requires:       perl(Complete::Util) >= 0.608
Requires:       perl(Exporter) >= 5.57
Requires:       perl(Getopt::Long) >= 2.50
Requires:       perl(Complete::Tcsh)
Requires:       perl(strict)
Requires:       perl(warnings)
BuildRequires:  perl >= 5.010001
BuildRequires:  perl(Complete::Bash) >= 0.333
BuildRequires:  perl(Complete::Getopt::Long) >= 0.479
BuildRequires:  perl(Complete::Util) >= 0.608
BuildRequires:  perl(Exporter) >= 5.57
BuildRequires:  perl(Getopt::Long) >= 2.50
BuildRequires:  perl(Complete::Tcsh)
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
This module extends L<Getopt::Long> with subcommands and tab completion
ability.

%prep

rm -rf %{_builddir}/Getopt-Long-Subcommand-%{version}
%setup -D -n Getopt-Long-Subcommand-0.104
chmod -R u+w %{_builddir}/Getopt-Long-Subcommand-%{version}

if [ -f pm_to_blib ]; then rm -f pm_to_blib; fi

%build

%{__perl} Makefile.PL OPTIMIZE="$RPM_OPT_FLAGS" INSTALLDIRS=vendor INSTALLSITEBIN=%{_bindir} INSTALLSITESCRIPT=%{_bindir} INSTALLSITEMAN1DIR=%{_mandir}/man1 INSTALLSITEMAN3DIR=%{_mandir}/man3 INSTALLSCRIPT=%{_bindir}
make %{?_smp_mflags}

#
# This is included here instead of in the 'check' section because
# older versions of rpmbuild (such as the one distributed with RHEL5)
# do not do 'check' by default.
#

if [ -z "$RPMBUILD_NOTESTS" ]; then
   make test
fi

%install

rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;
%{_fixperms} $RPM_BUILD_ROOT/*

%clean

rm -rf $RPM_BUILD_ROOT

%files

%defattr(-,root,root,-)
%{_bindir}/*
%{perl_vendorlib}/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Thu Aug 03 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> 0.104-1
- Generated using cpantorpm
